(require '[clojure.tools.cli :refer [parse-opts]])

(def cli-options
  ;; Defined CLI options here
  [["-f" "--file FILE"]
   ["-d" "--delimiter DELIMITER"]
   ["-o" "--out-names OUTNAMES" "Output file name list"]])

(defn split-multiple
  "Split multiple lines"
  [lines delimiter]
  (map #(str/split % (re-pattern delimiter)) lines))

(defn split-file
  "Split all lines in a file"
  [file delimiter]
  (split-multiple (str/split (slurp file) (re-pattern "\n")) delimiter))

(defn write-split-to-files
  "Write a split line into files"
  [filename line output-names]
  (loop [splits line
         out-names output-names]
    (when (seq splits)
      (do
        (let [out-filename (if (seq out-names)
                             (first out-names)
                             (str filename (- (count splits) (count line))))]
          (spit out-filename (str (first splits) "\n") :append true))
        (recur (rest splits) (rest out-names))))))

(defn split-into-multiple-files
  "Split a file into multiple different files"
  [input-file delimiter output-names]
  (let [split-file (split-file input-file delimiter)]
    (map #(write-split-to-files input-file % output-names) split-file)))

(defn parse-list
  "Parse a comma-separated string into a list"
  [string]
  (map #(str/trim %) (str/split string #",")))

(defn -main
  "The entry function"
  []
  (let [cli-opts (:options (parse-opts *command-line-args* cli-options))]
    (println cli-opts)
    ;;(split-file (:file cli-opts) (:delimiter cli-opts))))
    ;(println (:out-names cli-opts))
    (split-into-multiple-files (:file cli-opts) (:delimiter cli-opts) (parse-list (:out-names cli-opts)))))

(-main)
